from dataclasses import dataclass, fields
from typing import List, Optional, Tuple, Sequence
import pyarrow as pa
from pyarrow import parquet as pq, types


def get_parquet_type(_type):
    if _type == int:
        return pa.int64()
    if _type == float:
        return pa.float64()
    if _type == str:
        return pa.string()
    if _type.__name__.lower() == "list":
        if not hasattr(_type, "__args__"):
            raise ValueError("Must specify type of list elements")
        inner_type = get_parquet_type(_type.__args__[0])
        return pa.list_(inner_type)
    print(_type)
    if issubclass(_type, ParquetSchema):
        return pa.struct(pa.field(*f) for f in _type.to_schema())
    raise ValueError(f"Unsupported type {_type}")


class ParquetSchema():
    @classmethod
    def to_schema(cls):
        schema = []
        for f in fields(cls):
            _type = get_parquet_type(f.type)
            schema.append((f.name, _type))
        return schema

    def __iter__(self):
        for f in fields(self):
            yield f.name

    def __getitem__(self, k):
        if type(k) == int:
            k = fields(self)[k]
        return (k.name, getattr(self, k.name))

    def __len__(self):
        return len(fields(self))


def pivot_rows(rows: List[ParquetSchema]):
    if not rows:
        return []
    row = rows[0]
    for f in fields(row):
        yield [getattr(r, f.name) for r in rows]


@dataclass(frozen=True)
class Entity(ParquetSchema):
    name: str
    start_idx: int
    end_idx: int


@dataclass(frozen=True)
class Paragraph(ParquetSchema):
    text: str
    entities: List[Entity]
    #source: Optional[str]


@dataclass(frozen=True)
class Document(ParquetSchema):
    _id: str
    paragraphs: List[Paragraph]
#    hash: str


def build_df():
    row = Document(
        _id="alice1",
        paragraphs=[
            Paragraph(
                text=(
    "Alice was beginning to get very tired of sitting by her sister on the"
    "bank, and of having nothing to do: once or twice she had peeped into"
    "the book her sister was reading, but it had no pictures or"
    "conversations in it, “and what is the use of a book,” thought Alice"
    "“without pictures or conversations?”"
                ),
                entities=[
                    Entity("Alice", 0, 5),
                    Entity("Alice", 168, 173),
                ],
                #source="gutenberg",
            ),
            Paragraph(
                text=(
    "So she was considering in her own mind (as well as she could, for the"
    "hot day made her feel very sleepy and stupid), whether the pleasure of"
    "making a daisy-chain would be worth the trouble of getting up and"
    "picking the daisies, when suddenly a White Rabbit with pink eyes ran"
    "close by her."
                ),
                entities=[
                    Entity("White Rabbit", 203, 215),
                ],
                #source="gutenberg",
            ),
        ]
    )
    print(pa.schema(Document.to_schema()))
    df = pa.table(list(pivot_rows([row, row])), schema=pa.schema(Document.to_schema()))
    print(df)
    return df
    return pa.table(
        [
            [1, 1],
            [[1, 1], [1,1]],
        ],
        schema=pa.schema([
            ("id", pa.int64()),
            ("relations", pa.list_(pa.int64())),
        ])
    )


def write_documents(location: str):
    df = build_df()
    #print(df)


if __name__ == "__main__":
    write_documents("")

