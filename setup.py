from setuptools import find_packages
from setuptools import setup

setup(
    name="dask-dataclasses",
    packages=find_packages(),
    include_package_data=False,
    install_requires=[
        "pyarrow",
    ],
    extras_require={
        "complete": [
            "pandas",
        ],
        "testing": [
            "pytest~=7.3.0",
            "coverage~=7.2.0",
        ],
        "dev": [
            "pre-commit~=3.2.0",
            "identify>=2.2.4",
            "black==22.3.0",
            "flake8==5.0.4",
        ],
    },
)
